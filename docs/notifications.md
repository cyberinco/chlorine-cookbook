# 文本和语音提醒

## 响应文本和语音提醒
驾培终端可以通过平台来通知您。

响应文本提醒通知：如果您收到文本提醒通知时正好看着屏幕，只需轻点“关闭”。如果您稍后才看到通知，请轻点通知列表中的该通知，然后滚动并进行响应。
